import os
import json
import requests

OWNER = 'v2fly'
REPO = 'v2ray-core'

root = './v2ray/'

def get_latest(owner, repo):
    assert type(owner) is str
    assert type(repo) is str

    res = requests.get(f'https://api.github.com/repos/{owner}/{repo}/releases/latest')
    assert res.status_code == 200

    data = res.text
    data = json.loads(data)

    ver = data['tag_name']

    files = {}
    for it in data['assets']:
        name = it['name']
        url = it['browser_download_url']

        files[name] = url

    code = data['tarball_url']

    info = {'version': ver, 'files': files, 'code': code}
    return info

def check(info):
    assert type(info['version']) is str
    version = info['version']

    if os.path.isdir(f'{root}/{version}/'):
        raise FileExistsError

def download(info):
    version = info['version']
    files = info['files']
    code = info['code']

    assert type(version) is str
    assert type(files) is dict
    assert type(code) is str

    ls = f'{root}/{version}/'
    os.mkdir(ls)

    print('Version:', version)

    assets_path = ls + '/assets/'
    os.mkdir(assets_path)

    for name in files.keys():
        assert type(name) is str

        url = files[name]
        assert type(url) is str

        res = requests.get(url)
        assert res.status_code == 200

        data = res.content

        f = open(assets_path + f'/{name}', 'wb')
        f.write(data)
        f.close()

        print('Downloaded:', name)

    res = requests.get(code)
    assert res.status_code == 200

    code_data = res.content

    f = open(f'{ls}/source-code.tar.gz', 'wb')
    f.write(code_data)
    f.close()

    print('Downloaded Source Code!')

def main():
    info = get_latest(OWNER, REPO)
    print(json.dumps(info, indent=2))

    check(info)
    download(info)

main()

